package com.gitlab.firelight.kilnreborn.mixin;

import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.data.recipes.SimpleCookingRecipeBuilder;
import net.minecraft.world.item.crafting.AbstractCookingRecipe;
import net.minecraft.world.item.crafting.CookingBookCategory;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.ItemLike;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

@Mixin(SimpleCookingRecipeBuilder.class)
public interface SimpleCookingRecipeBuilderAccessor {
    @Invoker("<init>")
    static SimpleCookingRecipeBuilder createSimpleCookingRecipeBuilder(
        RecipeCategory recipeCategory,
        CookingBookCategory cookingBookCategory,
        ItemLike itemLike,
        Ingredient ingredient,
        float f,
        int i,
        AbstractCookingRecipe.Factory<?> factory
    ) {
        throw new UnsupportedOperationException();
    }
}
