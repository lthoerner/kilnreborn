package com.gitlab.firelight.kilnreborn.client;

import com.gitlab.firelight.kilnreborn.client.screen.KilnScreen;
import com.gitlab.firelight.kilnreborn.registry.KilnScreenHandlerInit;
import net.fabricmc.api.ClientModInitializer;
import net.minecraft.client.gui.screens.MenuScreens;

public class KilnRebornClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        MenuScreens.register(KilnScreenHandlerInit.KILN_SCREEN_HANDLER, KilnScreen::new);
    }
}
