package com.gitlab.firelight.kilnreborn.compat.rei;

import com.gitlab.firelight.kilnreborn.KilnReborn;
import com.gitlab.firelight.kilnreborn.recipe.FiringRecipe;
import com.gitlab.firelight.kilnreborn.registry.KilnItemInit;
import com.gitlab.firelight.kilnreborn.registry.KilnRecipeInit;
import me.shedaniel.rei.api.client.plugins.REIClientPlugin;
import me.shedaniel.rei.api.client.registry.category.CategoryRegistry;
import me.shedaniel.rei.api.client.registry.display.DisplayRegistry;
import me.shedaniel.rei.api.common.category.CategoryIdentifier;
import me.shedaniel.rei.api.common.util.EntryStacks;
import me.shedaniel.rei.plugin.client.categories.cooking.DefaultCookingCategory;
import me.shedaniel.rei.plugin.common.displays.cooking.DefaultSmeltingDisplay;

public class KilnREIClientPlugin implements REIClientPlugin {
    public static final CategoryIdentifier<DefaultSmeltingDisplay> FIRING = CategoryIdentifier.of(KilnReborn.MOD_ID, "firing");

    @Override
    public void registerCategories(CategoryRegistry registry) {
        registry.add(new DefaultCookingCategory(FIRING, EntryStacks.of(KilnItemInit.KILN), "category.kilnreborn.firing"));

        registry.addWorkstations(FIRING, EntryStacks.of(KilnItemInit.KILN));
    }

    @Override
    public void registerDisplays(DisplayRegistry registry) {
        registry.registerRecipeFiller(FiringRecipe.class, KilnRecipeInit.FIRING, DefaultFiringDisplay::new);
    }
}
