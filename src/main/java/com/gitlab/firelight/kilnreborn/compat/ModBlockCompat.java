package com.gitlab.firelight.kilnreborn.compat;

public class ModBlockCompat {
    public static final String[] naturalProgressionCobbledBlocks = new String[] { "andesite", "diorite", "granite", "sandstone", "red_sandstone", "tuff", "dripstone_block", "netherrack", "end_stone" };
}
