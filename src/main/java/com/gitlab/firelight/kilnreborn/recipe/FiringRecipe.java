package com.gitlab.firelight.kilnreborn.recipe;

import com.gitlab.firelight.kilnreborn.registry.KilnRecipeInit;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.AbstractCookingRecipe;
import net.minecraft.world.item.crafting.CookingBookCategory;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;

public class FiringRecipe extends AbstractCookingRecipe {
    public FiringRecipe(String string, CookingBookCategory category, Ingredient ingredient, ItemStack itemStack, float f, int i) {
        super(KilnRecipeInit.FIRING, string, category, ingredient, itemStack, f, i);
    }

    @Override
    public RecipeSerializer<?> getSerializer() {
        return KilnRecipeInit.FIRING_SERIALIZER;
    }

    @Override
    public boolean isSpecial() {
        return true;
    }
}
