package com.gitlab.firelight.kilnreborn.registry;

import com.gitlab.firelight.kilnreborn.KilnReborn;
import com.gitlab.firelight.kilnreborn.block.screen.KilnScreenHandler;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.inventory.MenuType;

public class KilnScreenHandlerInit {
    public static final MenuType<KilnScreenHandler> KILN_SCREEN_HANDLER = Registry.register(BuiltInRegistries.MENU, new ResourceLocation(KilnReborn.MOD_ID, "kiln"), new MenuType<KilnScreenHandler>(KilnScreenHandler::new, FeatureFlags.VANILLA_SET));

    public static void loadScreens() {
    }
}
