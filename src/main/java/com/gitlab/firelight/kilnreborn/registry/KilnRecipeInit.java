package com.gitlab.firelight.kilnreborn.registry;

import com.gitlab.firelight.kilnreborn.KilnReborn;
import com.gitlab.firelight.kilnreborn.recipe.FiringRecipe;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.item.crafting.SimpleCookingSerializer;

public class KilnRecipeInit {
    public static final RecipeType<FiringRecipe> FIRING = RecipeType.register("firing");
    public static final SimpleCookingSerializer<FiringRecipe> FIRING_SERIALIZER = Registry.register(BuiltInRegistries.RECIPE_SERIALIZER, new ResourceLocation(KilnReborn.MOD_ID, "firing"), new SimpleCookingSerializer<>(FiringRecipe::new, 100));

    public static void loadRecipes() {
    }
}
