package com.gitlab.firelight.kilnreborn.registry;

import com.gitlab.firelight.kilnreborn.KilnReborn;
import com.gitlab.firelight.kilnreborn.block.custom.KilnBlock;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;

public class KilnBlockInit {
    public static final Block KILN = Registry.register(BuiltInRegistries.BLOCK, new ResourceLocation(KilnReborn.MOD_ID, "kiln"), new KilnBlock(Blocks.BRICKS));

    public static void loadBlocks() {

    }
}
