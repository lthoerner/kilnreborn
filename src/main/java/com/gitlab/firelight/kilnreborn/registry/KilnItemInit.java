package com.gitlab.firelight.kilnreborn.registry;

import com.gitlab.firelight.kilnreborn.KilnReborn;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;

public class KilnItemInit {
    public static final Item KILN = Registry.register(BuiltInRegistries.ITEM, new ResourceLocation(KilnReborn.MOD_ID, "kiln"), new BlockItem(KilnBlockInit.KILN, new Item.Properties()));

    public static void registerItems() {
        ItemGroupEvents.modifyEntriesEvent(CreativeModeTabs.FUNCTIONAL_BLOCKS).register(entries -> {
            entries.accept(KILN);
        });
    }
}
