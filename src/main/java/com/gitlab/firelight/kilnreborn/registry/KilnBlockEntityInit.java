package com.gitlab.firelight.kilnreborn.registry;

import com.gitlab.firelight.kilnreborn.KilnReborn;
import com.gitlab.firelight.kilnreborn.block.entity.KilnBlockEntity;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.entity.BlockEntityType;

public class KilnBlockEntityInit {
    public static final BlockEntityType<KilnBlockEntity> KILN_BLOCK_ENTITY = Registry.register(BuiltInRegistries.BLOCK_ENTITY_TYPE, new ResourceLocation(KilnReborn.MOD_ID, "kiln"), FabricBlockEntityTypeBuilder.create(KilnBlockEntity::new, KilnBlockInit.KILN).build(null));

    public static void loadBlockEntities() {

    }
}
