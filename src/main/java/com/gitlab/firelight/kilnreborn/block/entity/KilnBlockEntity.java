package com.gitlab.firelight.kilnreborn.block.entity;

import com.gitlab.firelight.kilnreborn.block.screen.KilnScreenHandler;
import com.gitlab.firelight.kilnreborn.registry.KilnBlockEntityInit;
import com.gitlab.firelight.kilnreborn.registry.KilnRecipeInit;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.AbstractFurnaceBlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class KilnBlockEntity extends AbstractFurnaceBlockEntity {
    public KilnBlockEntity(BlockPos blockPos, BlockState blockState) {
        super(KilnBlockEntityInit.KILN_BLOCK_ENTITY, blockPos, blockState, KilnRecipeInit.FIRING);
    }

    @Override
    protected Component getDefaultName() {
        return Component.translatable("block.kilnreborn.kiln");
    }

    @Override
    protected int getBurnDuration(ItemStack itemStack) {
        return super.getBurnDuration(itemStack) / 2;
    }

    @Override
    protected AbstractContainerMenu createMenu(int i, Inventory inventory) {
        return new KilnScreenHandler(i, inventory, this, this.dataAccess);
    }
}
